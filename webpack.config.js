const path = require("path");
const fs = require("fs");
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: getEntrypoints(),
    output: {
        filename: "[name]/[name]Handler.js",
        libraryTarget: "commonjs",
        path: path.resolve("./infrastructure/bin/")
    },
    devtool: process.env.NODE_ENV === "production" ? false : "inline-source-map",
    resolve: {
        extensions: [".ts", ".js"]
    },
    target: "node",
    mode: process.env.NODE_ENV === "production" ? "production" : "development",
    module: {
        rules: [{ loader: "ts-loader", test: /\.(ts|tsx)$/ }]
    },
    plugins: [new ForkTsCheckerWebpackPlugin()]
};

function getEntrypoints() {
    const entryPoints = {};

    const expectedHandlers = fs.readdirSync("./src/handlers");

    for (const handlerName of expectedHandlers) {
        const expectedHandlerPath = `./src/handlers/${handlerName}/${handlerName}Handler.ts`;

        if (fs.existsSync(expectedHandlerPath)) {
            entryPoints[handlerName] = expectedHandlerPath;
        } else {
            console.error(`Lambda handler ${expectedHandlerPath} does not exist - ensure the handler name matches the folder convention.`);
            process.exit(1);
        }
    }

    return entryPoints;
}
