AWSTemplateFormatVersion: "2010-09-09"

Transform: AWS::Serverless-2016-10-31

Globals:
  Function:
    Timeout: 60
    MemorySize: 2048
    Architectures:
      - arm64
    Tracing: Active

Parameters:
  EventBusName:
    Type: String
    Default: "default"
  EventBusArn:
    Type: String
    Default: "*"
  DeploymentEnvironment:
    Type: String
    Default: "sbx"
  Prefix:
    Type: String
    Default: "main"
  ComponentVersion:
    Type: String
    Default: "1.0.0"

Resources:
  ####################################################
  ######### Function & State Machine
  ####################################################
  StateMachine:
    Type: AWS::Serverless::StateMachine
    UpdateReplacePolicy: Delete
    DeletionPolicy: Delete
    Properties:
      Name: !Sub "${Prefix}-pets-statemachine"
      DefinitionUri: statemachine/pets.asl.yaml
      Role: !GetAtt StateMachineFunctionRole.Arn
      Type: "EXPRESS"
      Logging:
        Destinations:
          - CloudWatchLogsLogGroup:
              LogGroupArn: !GetAtt StepFunctionLogGroup.Arn
        IncludeExecutionData: true
        Level: ALL

  StepFunctionLogGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      RetentionInDays: 30

  LogGroupPolicyModifierHandlerWorking:
    Type: AWS::Serverless::Function
    Properties:
      FunctionName: !Sub "${Prefix}-working"
      Description: !Sub "${Prefix} Function - Adds the needed policys to the state machines log group"
      CodeUri: ./bin/logGroupPolicyModifier/
      Handler: logGroupPolicyModifierHandler.handler
      Runtime: nodejs18.x
      Environment:
        Variables:
          LOG_GROUP_ARN: !GetAtt StepFunctionLogGroup.Arn
          POLICY_NAME: !Sub "${Prefix}-log-group-policy"
      Role: !GetAtt LambdaPolicyModifierRoleWorking.Arn

  LambdaPolicyModifierRoleWorking:
    Type: AWS::IAM::Role
    Properties:
      Description: The execution role for the Publish Model Lambda function.
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
        - arn:aws:iam::aws:policy/CloudWatchLogsFullAccess
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service: lambda.amazonaws.com
            Action: sts:AssumeRole
        
  LogGroupPolicyModifierHandlerExecutorWorking:
    Type: AWS::CloudFormation::CustomResource
    Properties:
      ServiceToken: !GetAtt LogGroupPolicyModifierHandlerWorking.Arn

  LogGroupPolicyModifierHandlerBroken:
    Type: AWS::Serverless::Function
    Properties:
      FunctionName: !Sub "${Prefix}-broken"
      Description: !Sub "${Prefix} Function - Adds the needed policys to the state machines log group"
      CodeUri: ./bin/logGroupPolicyModifier/
      Handler: logGroupPolicyModifierHandler.handler
      Runtime: nodejs18.x
      Environment:
        Variables:
          LOG_GROUP_ARN: !GetAtt StepFunctionLogGroup.Arn
          POLICY_NAME: !Sub "${Prefix}-log-group-policy"
      Role: !GetAtt LambdaPolicyModifierRoleBroken.Arn

  LambdaPolicyModifierRoleBroken:
    Type: AWS::IAM::Role
    Properties:
      Description: The execution role for the Publish Model Lambda function.
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service: lambda.amazonaws.com
            Action: sts:AssumeRole
      Policies:
        - PolicyName: InlinePutEventsPolicy
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - events:PutEvents
                Resource: !Ref EventBusArn
              - Effect: Allow
                Action:
                  - 'logs:PutResourcePolicy'
                  - 'logs:DeleteResourcePolicy'
                Resource: !GetAtt StepFunctionLogGroup.Arn

  LogGroupPolicyModifierHandlerExecutorBroken:
    Type: AWS::CloudFormation::CustomResource
    Properties:
      ServiceToken: !GetAtt LogGroupPolicyModifierHandlerBroken.Arn

  StateMachineFunctionRole:
    Type: AWS::IAM::Role
    Properties:
      Description: The execution role for the Publish Model Lambda function.
      ManagedPolicyArns: #TODO: Least privilege
        - arn:aws:iam::aws:policy/AWSLambda_FullAccess
        - arn:aws:iam::aws:policy/IAMFullAccess
      Policies:
        - PolicyName: InlinePutEventsPolicy
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - events:PutEvents
                Resource: !Ref EventBusArn
              - Effect: Allow
                Action:
                  - 'logs:CreateLogDelivery'
                  - 'logs:GetLogDelivery'
                  - 'logs:UpdateLogDelivery'
                  - 'logs:DeleteLogDelivery'
                  - 'logs:ListLogDeliveries'
                  - 'logs:PutResourcePolicy'
                  - 'logs:DescribeResourcePolicies'
                Resource: '*'
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service: states.amazonaws.com
            Action: sts:AssumeRole

Outputs:
  StateMachineArn:
    Value: !GetAtt StateMachine.Arn