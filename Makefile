ifndef VERBOSE
MAKEFLAGS += --no-print-directory
endif
SHELL := /usr/bin/env bash
.DEFAULT_GOAL := validate

# ====
# NOTE
# it's currently easier to just support bash on windows on windows for now
# this won't work on PowerShell etc, unless you manually alter the vars to your liking
# you can also alter the ones setting via '?=' with normal ENV vars
# eg: export GIT_USER_SHORT=nick
# ====

# ==== vars ====
# AWS
AWS_PROFILE ?= default
AWS_REGION = eu-west-1

# stacks
GIT_USER ?= $(shell git config user.name)
GIT_USER_SHORT ?= $(shell git config user.email | tr @ . | cut -d'.' -f 1 | tr '[:upper:]' '[:lower:]' | cut -c-6)
STACK_SUFFIX ?= ${shell git symbolic-ref HEAD | sed -e 's,refs/heads/\(.*\),\1,' | cut -d'/' -f 2-3 | tr '[:upper:]' '[:lower:]' | cut -c-6}

ifeq ($(STACK_SUFFIX),)
$(info ===)
$(info Windows PS / cmd prompt is not supported)
$(info Please use Git Bash)
$(info ===)
exit
endif

# main iac stack
MAIN_STACK_NAME := test-stack-final
LOCAL_ENDPOINTS_PATH := src/handlers/dataSetup/endpoints.json

# tags
TAGS_CUSTOMER := Game Technology
TAGS_PROJECT := Games Promotional Tools
TAGS_PROJECT_CODE := gppt
TAGS_VERSION := $(shell echo 123456)
TAGS_TIMESTAMP := $(shell date +"%FT%T%z")

# ==== end vars ====

$(info AWS Profile:                   $(AWS_PROFILE))
$(info AWS Region:                    $(AWS_REGION))
$(info Git User:                      $(GIT_USER))
$(info Git User short:                $(GIT_USER_SHORT))
$(info Git Branch / Suffix:           $(STACK_SUFFIX))
$(info Mock API Stack:        $(MOCK_API_STACK_NAME))
$(info Mock API URL:                  $(MOCK_API_URL))
$(info Main Stack:                    $(MAIN_STACK_NAME))
$(info )

all: build deploy

build:
	npx webpack-cli --node-env development
	sam build --template infrastructure/template.yaml

deploy:
	sam deploy \
		--profile $(AWS_PROFILE) \
		--region $(AWS_REGION) \
		--stack-name $(MAIN_STACK_NAME) \
		--parameter-overrides DeploymentEnvironment=sbx DeploymentTimestamp=$(TAGS_TIMESTAMP) ComponentVersion='$(TAGS_VERSION)'\
		--capabilities CAPABILITY_IAM CAPABILITY_AUTO_EXPAND \
		--resolve-s3 \
		--no-fail-on-empty-changeset \
		--tags environment=sbx author='$(GIT_USER)' customer='$(TAGS_CUSTOMER)' project='$(TAGS_PROJECT)' project-code='$(TAGS_PROJECT_CODE)' version='$(TAGS_VERSION)' \
		--no-confirm-changeset \
		--no-fail-on-empty-changeset

destroy: delete_stack

delete_stack:
	sam delete \
		--profile $(AWS_PROFILE) \
		--region $(AWS_REGION) \
		--stack-name $(MAIN_STACK_NAME) \
		--no-prompts
