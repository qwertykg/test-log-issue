import { CloudFormationCustomResourceEvent, CloudFormationCustomResourceResponse, Context } from "aws-lambda";
import {
    CloudWatchLogsClient,
    DeleteResourcePolicyCommand,
    DeleteResourcePolicyRequest,
    PutResourcePolicyCommand,
    PutResourcePolicyRequest
} from "@aws-sdk/client-cloudwatch-logs";

export const handler = async (event: CloudFormationCustomResourceEvent, context: Context): Promise<void> => {
    var cloudWatchClient = new CloudWatchLogsClient({});

    console.log("ARN given is:");
    console.log(process.env["LOG_GROUP_ARN"]);

    if (event.RequestType == "Create") putPolicy(process.env["LOG_GROUP_ARN"] || "", process.env["POLICY_NAME"] || "",cloudWatchClient);
    if (event.RequestType == "Delete") deletePolicy(process.env["POLICY_NAME"] || "", cloudWatchClient);

    await sendResponse(true, event, context, "The policy was added to the role sucessfully");
};

async function putPolicy(logGroupArn: string, policyName: string, cloudWatchClient: CloudWatchLogsClient): Promise<void> {
    const input: PutResourcePolicyRequest = {
        policyName: policyName,
        policyDocument: `{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":{\"Service\":\"delivery.logs.amazonaws.com\"},\"Action\":[\"logs:CreateLogStream\",\"logs:PutLogEvents\"],\"Resource\":\"${logGroupArn}\"}]}`
    };

    const command = new PutResourcePolicyCommand(input);

    await cloudWatchClient.send(command);
}

async function deletePolicy(policyName: string, cloudWatchClient: CloudWatchLogsClient): Promise<void> {
    const input: DeleteResourcePolicyRequest = {
        policyName: policyName
    };
    const command = new DeleteResourcePolicyCommand(input);

    await cloudWatchClient.send(command);
}

async function sendResponse(success: boolean, event: CloudFormationCustomResourceEvent, context: Context, reason: string): Promise<void> {
    const responseBody: CloudFormationCustomResourceResponse = {
        Status: success ? "SUCCESS" : "FAILED",
        Reason: reason,
        PhysicalResourceId: context.logStreamName,
        StackId: event.StackId,
        RequestId: event.RequestId,
        LogicalResourceId: event.LogicalResourceId
    };

    const init: RequestInit = {
        method: "PUT",
        body: JSON.stringify(responseBody),
        headers: {}
    };

    await fetch(event.ResponseURL, init);
    
}
